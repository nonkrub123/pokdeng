import React from 'react';
import styles from './CardComponent.module.scss';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { GameAction, GameSelector } from '../../store/game/gameSlice';
import { OnHandCard } from 'src/store/game/gameModel';

type Props = {
  cardIndex: number,
  gamblerIndex: number,
  card: OnHandCard | null,
};

const CardComponent = (props: Props): JSX.Element => {
  const dispatch = useAppDispatch();
  const scene = useAppSelector(GameSelector.scene);
  const displayCard = props.card
    ? (props.card.open
      ? styles[`${props.card.suit.toLowerCase()}-${props.card.symbol.toLowerCase()}`]
      : styles.back)
    : styles.empty;

  const handleClick = () => {
    dispatch(GameAction.openCard({
      gamblerIndex: props.gamblerIndex,
      cardIndex: props.cardIndex
    }));
  };

  return (
    props.gamblerIndex === 0 && (scene === 'DEAL' || scene === 'DRAW')
      ? <div className={`${!props.card?.open && displayCard === styles.back ? styles.hover : ''} ${displayCard}`} onClick={handleClick}></div>
      : <div className={displayCard}></div>
  );
};

export default CardComponent;
