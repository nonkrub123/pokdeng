import React from 'react';
import styles from './BetModalComponent.module.scss';
import Modal from 'react-bootstrap/Modal';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { GameAction, GameSelector } from '../../store/game/gameSlice';
import type { Ref } from '../modal/ref';
import BetFormComponent from './BetFormComponent';

type Props = {
  coins: number,
};

const BetModalComponent = React.forwardRef<Ref, Props>((props, ref) => {
  React.useImperativeHandle(ref, () => ({
    show: handleShow,
    hide: handleClose,
  }));

  const dispatch = useAppDispatch();
  const gamblers = useAppSelector(GameSelector.gamblers);

  const [show, setShow] = React.useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const handleSelect = async (bet: number) => {
    if (bet) {
      dispatch(GameAction.placeBet({ gamblerIndex: 0, bet: bet }));
      dispatch(GameAction.placeBet({ gamblerIndex: 1, bet: gamblerBet(1) }));
      dispatch(GameAction.placeBet({ gamblerIndex: 2, bet: gamblerBet(2) }));
      dispatch(GameAction.changeScene('BET'));
      handleClose();
    }
  };

  const gamblerBet = (gamblerIndex: number) => {
    const gambler = gamblers[gamblerIndex];
    const bet = Math.floor((Math.random() * 10) + 1) * 10;

    return gambler.coins > bet
      ? bet
      : gambler.coins;
  };

  return (
    <Modal
      ref={ref}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      scrollable={true}
    >
      <Modal.Header closeButton>
        <h5 className="modal-title">
          <span className="fa fa-coins me-2"></span>เดิมพัน
        </h5>
      </Modal.Header>
      <Modal.Body>
        <BetFormComponent
          coins={props.coins}
          onSelect={handleSelect}
        />
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={handleClose}
        >
          <span className="fas fa-fw fa-ban me-2"></span>ยกเลิก
        </button>
      </Modal.Footer>
    </Modal>
  );
});

BetModalComponent.displayName = 'BetModal';
export default BetModalComponent;
