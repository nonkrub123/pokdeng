import React from 'react';
import styles from './ControlComponent.module.scss';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { GameAction, GameSelector } from '../../store/game/gameSlice';
import MessageComponent from '../message/MessageComponent';
import Swal from 'sweetalert2';

const DealControlComponent = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const deck = useAppSelector(GameSelector.deck);
  const points = useAppSelector(GameSelector.points);
  const gamblers = useAppSelector(GameSelector.gamblers);
  const player = gamblers[0];
  const dealer = gamblers[3];
  const isCardsOpened = player.cards.filter(c => !c.open).length === 0;

  const handleCheckClick = async () => {
    const dealerPoint = points[3].point;
    const lucky = Math.floor((Math.random() * 10));
    console.log(`lucky: ${lucky}`);

    if (dealerPoint >= 8) {
      dealerPok();
    } else if (lucky >= 6) {
      swapCard(lucky);
      dealerPok();
    } else {
      dealerDontPok();
    }
  };

  const dealerPok = async () => {
    await Swal.fire({
      title: 'เจ้ามือป๊อก',
      text: 'โปรดแสดงไพ่ในมือเพื่อนับแต้ม',
      icon: 'error',
      confirmButtonColor: '#f86c6b',
    });

    gamblers.map((gambler, gamblerIndex) => {
      gambler.cards.map((_, cardIndex) => {
        dispatch(GameAction.openCard({ gamblerIndex: gamblerIndex, cardIndex: cardIndex }));
      });
    });

    dispatch(GameAction.changeScene('FINISH'));
  };

  const dealerDontPok = async () => {
    await Swal.fire({
      title: 'เจ้ามือไม่ป๊อก',
      text: 'ยินดีด้วย ดูเหมือนคุณยังมีโอกาสอยู่',
      icon: 'info',
      confirmButtonColor: '#0d6efd',      
    });

    dispatch(GameAction.changeScene('CHECK'));
  };

  const swapCard = (lucky: number) => {
    const point = lucky > 8 ? 9 : 8;
    const firstPoint = dealer.cards[0].point;
    const secondCard = dealer.cards[1];
    const target = point - firstPoint;
    const cheat = deck.find(card => card.point === target);
    const cheatBackup = deck.find(card => card.point === target - 1);  // เผื่อไม่พบไพ่ใบแรกที่ต้องการ
    const cardInhand = { suit: secondCard.suit, symbol: secondCard.symbol, point: secondCard.point };
    const cardInDeck = !cheat ? cheatBackup : cheat;
    dispatch(GameAction.swapCard({ gamblerIndex: 3, cardInhand: cardInhand, cardInDeck: cardInDeck! }));
  };

  return isCardsOpened
    ? (<div className="d-grid gap-2">
        <button
          className="btn btn-primary"
          onClick={handleCheckClick}
        >
          ถัดไป
        </button>
      </div>)
    : (<MessageComponent message="คลิกที่ไพ่เพื่อดูแต้มที่ได้" />);
};

export default DealControlComponent;
