import React from 'react';
import styles from './DrawControlComponent.module.scss';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { GameAction, GameSelector } from '../../store/game/gameSlice';
import MessageComponent from '../message/MessageComponent';

const DrawControlComponent = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const gamblers = useAppSelector(GameSelector.gamblers);
  const player = gamblers[0];
  const isCardsOpened = player.cards.filter(c => !c.open).length === 0;

  const handleCheck2Click = () => {
    gamblers.map((gambler, gamblerIndex) => {
      gambler.cards.map((_, cardIndex) => {
        dispatch(GameAction.openCard({ gamblerIndex: gamblerIndex, cardIndex: cardIndex }));
      });
    });
    
    dispatch(GameAction.changeScene('FINISH'));
  };

  return isCardsOpened
    ? (<div className="d-grid gap-2">
        <button
          className="btn btn-primary"
          onClick={handleCheck2Click}
        >
          สรุปแต้ม
        </button>
      </div>)
    : (<MessageComponent message="คลิกที่ไพ่เพื่อดูแต้มที่ได้" />);
};

export default DrawControlComponent;
