import React from 'react';
import styles from './DealerComponent.module.scss';
import { useAppSelector } from '../../hooks/redux';
import { GameSelector } from '../../store/game/gameSlice';
import CardComponent from '../../components/card/CardComponent';
import MessageComponent from '../../components/message/MessageComponent';

const DealerComponent = (): JSX.Element => {
  const scene = useAppSelector(GameSelector.scene);
  const gamblers = useAppSelector(GameSelector.gamblers);
  const dealer = gamblers.length > 0
    ? gamblers[3]
    : { name: '???', aka: '?????', cards: [], coins: 0, bet: 0, criminals: [] };
  const points = useAppSelector(GameSelector.points);
  const [message, setMessage] = React.useState('');

  React.useEffect(() => {
    switch (scene) {
      case 'START':
        setMessage('');
        break;
      case 'FINISH':
        setMessage(toMessage(points[3].point, points[3].deng));
        break;
    }
  }, [scene]);

  const toMessage = (point: number, deng: number) => {
    const messagePoint = point >= 8 ? `ป๊อก ${point}` : `${point} แต้ม`;
    const messageDeng = deng > 0 ? `, ${deng} เด้ง` : '';
    return point > 0 ? `${messagePoint}${messageDeng}` : 'บอด';
  };

  return (
    <div className={`${styles.card} card mb-3`}>
      <div className="card-body">
        <h5 className="card-title">
          <span className="fa fa-user-secret me-2"></span>{dealer.name}
        </h5>
        <h6 className="card-subtitle mb-2 text-muted">
          {dealer.aka}
        </h6>
        <div className="d-flex justify-content-end mb-3">
          <span className="text-warning fa fa-coins me-2"></span>
          <span className="fa fa-infinity"></span>
        </div>
        <div className="row mb-2">
          {[...Array(3)].map((_, index) =>
            <div key={index} className="col-4 d-flex justify-content-center">
              <CardComponent
                gamblerIndex={3}
                cardIndex={index}
                card={dealer.cards.length > index ? dealer.cards[index] : null}
              />
            </div>
          )}
        </div>
        <MessageComponent message={message} />
      </div>
    </div>
  );
};

export default DealerComponent;
