import React from 'react';
import styles from './GamblerComponent.module.scss';
import { useAppSelector } from '../../hooks/redux';
import { GameSelector } from '../../store/game/gameSlice';
import type { Ref } from '../modal/ref';
import CardComponent from '../../components/card/CardComponent';
import MessageComponent from '../../components/message/MessageComponent';
import CriminalModalComponent from '../criminal/CriminalModalComponent';

type Props = {
  gamblerIndex: number,
};

const GamblerComponent = (props: Props): JSX.Element => {
  const scene = useAppSelector(GameSelector.scene);
  const gamblers = useAppSelector(GameSelector.gamblers);
  const gambler = gamblers.length > 0
    ? gamblers[props.gamblerIndex]
    : { name: '???', aka: '?????', cards: [], coins: 0, bet: 0, criminals: [] };
  const points = useAppSelector(GameSelector.points);
  const [message, setMessage] = React.useState('');
  const criminalModal = React.useRef<Ref>(null);

  React.useEffect(() => {
    switch (scene) {
      case 'START':
        setMessage('');
        break;
      case 'FINISH':
        setMessage(toMessage(points[props.gamblerIndex].point, points[props.gamblerIndex].deng));
        break;
    }
  }, [scene]);

  const handleCriminalClick = () => {
    criminalModal.current?.show();
  };

  const toMessage = (point: number, deng: number) => {
    const dealerPoint = points[3].point;
    const messagePoint = point >= 8 ? `ป๊อก ${point}` : `${point} แต้ม`;
    const messageDeng = deng > 0 ? `, ${deng} เด้ง` : '';
    const score = point > 0 ? `${messagePoint}${messageDeng}` : 'บอด';
    return `${dealerPoint >= point ? (dealerPoint === point ? '[เสมอ]' : '[แพ้]') : '[ชนะ]'} ${score}`;
  };

  return (<>
    <div className={`${styles.card} card mb-3`}>
      <div className="card-body">
        <div className="row">
          <div className="col-6">
            <h5 className="card-title">
            <span className="fa fa-user-ninja me-2"></span>{gambler.name}
          </h5>
          <h6 className="card-subtitle mb-2 text-muted">{gambler.aka}</h6>
          </div>
          <div className="col-6 d-flex justify-content-end">
            <h1>{gambler.bet}</h1>
          </div>
        </div>
        <div className="d-flex justify-content-between mb-1">
          <button
            className="btn btn-outline-danger btn-sm"
            disabled={gambler.criminals.length === 0}
            onClick={handleCriminalClick}
          >
            <span className="fa fa-skull me-2"></span>
            <span>{gambler.criminals.length}</span>
          </button>
          <div className="d-flex align-items-center">
            <span className="text-warning fa fa-coins me-2"></span>
            <span>{gambler.coins}</span>
          </div>
        </div>
        <div className="row mb-2">
          {[...Array(3)].map((_, index) =>
            <div key={index} className="col-4 d-flex justify-content-center">
              <CardComponent
                gamblerIndex={props.gamblerIndex}
                cardIndex={index}
                card={gambler.cards.length > index ? gambler.cards[index] : null}
              />
            </div>
          )}
        </div>
        <MessageComponent message={message} />
      </div>
    </div>

    {/* Modals */}
    <CriminalModalComponent
      ref={criminalModal}
    />
  </>);
};

export default GamblerComponent;
