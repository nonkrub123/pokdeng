import React from 'react';
import styles from './MessageComponent.module.scss';

type Props = {
  message: string,
};

const MessageComponent = (props: Props): JSX.Element => {
  return (<input
      className={`form-control text-light ${styles.message}`}
      type="text"
      value={props.message}
      readOnly
    />);
};

export default MessageComponent;