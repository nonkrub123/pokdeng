export type Ref = {
  show: () => void;
  hide: () => void;
}
