import React from 'react';
import styles from './NotFoundPage.module.scss';
import { useNavigate } from 'react-router-dom';

const NotFoundPage = (): JSX.Element => {
  const navigate = useNavigate();

  return (
    <div className={`${styles.page404} d-flex justify-content-center align-items-center`}>
      <div className={`card ${styles['card-tranparent']}`}>
        <div className="card-body text-center px-5 py-4">
          <h1 className={`display-3 mr-4 ${styles['page404-title']}`}>404</h1>
          <h4 className="pt-3">Oops! You&apos;re lost.</h4>
          <p className="text-muted float-left">
            The page you are looking for was not found.
          </p>
          <a href="#" onClick={(e) => { e.preventDefault(); navigate(-1); }}>Go Back</a>
        </div>
      </div>
    </div>
  );
};

export default NotFoundPage;
