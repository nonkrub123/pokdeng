import { Card, Suit, Symbol, Point } from './gameModel';

export interface DeckService {
  initialDeck: () => Card[];
}

const initialDeck = (): Card[] => {
  const deck = [] as Card[];
  const suits = ['SPADE', 'HEART', 'DIAMOND', 'CLUB'];
  const symbols = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];

  suits.forEach(suit => {
    symbols.forEach((symbol, index) => {
      const card: Card = {
        suit: suit as Suit,
        symbol: symbol as Symbol,
        point: (index < 9 ? index + 1 : 0) as Point
      };

      deck.push(card);
    });
  });

  return shuffleDeck(deck, 3);
};

const shuffleDeck = (deck: Card[], loop: number): Card[] => {
  let currentIndex = deck.length;
  let randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex != 0) {
    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [deck[currentIndex], deck[randomIndex]] = [deck[randomIndex], deck[currentIndex]];
  }

  return loop - 1 > 0 ? shuffleDeck(deck, loop - 1) : deck;
};

export default { initialDeck } as DeckService;
