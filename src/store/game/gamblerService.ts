import { Gambler } from './gameModel';

export interface GamblerService {
  initialGamblers: () => Gambler[];
}

const initialGamblers = (): Gambler[] => {
  const gamblers: Gambler[] = [
    {
      name: 'น้องกร๊วก',
      aka: 'อะปัดติเย',
      cards: [],
      coins: 100,
      bet: 0,
      criminals: []
    },
    {
      name: 'ป้าป้อม',
      aka: 'ซีอิ้วตราเด็กสมบูรณ์',
      cards: [],
      coins: 100,
      bet: 0,
      criminals: []
    },
    {
      name: 'หนูหริ่ง',
      aka: 'สมุนไพรคิมิโนโตะ',
      cards: [],
      coins: 100,
      bet: 0,
      criminals: []
    },
    {
      name: 'ตุ๊ดตู่',
      aka: 'เจ้ามือนะจ๊ะ',
      cards: [],
      coins: 0,
      bet: 0,
      criminals: []
    },
  ];

  return gamblers;
};

export default { initialGamblers } as GamblerService;
