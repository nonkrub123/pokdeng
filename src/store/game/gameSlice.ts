import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../';
import deckService from './deckService';
import gamblerService from './gamblerService';
import { Scene, Card, Gambler, Criminal } from './gameModel';

// namespace
export const GAME_STORE_NAMESPACE = 'gameStore';

// state
interface GameState {
  scene: Scene;
  deck: Card[];
  gamblers: Gambler[];
}

const initialState: GameState = {
  scene: 'SLEEP',
  deck: [],
  gamblers: [],
};

// reducer
const reducers = {
  newGame: (state: GameState) => {
    state.scene = 'START';
    state.deck = deckService.initialDeck();
    state.gamblers = gamblerService.initialGamblers();
  },
  resetGame: (state: GameState) => {
    state.scene = 'START';
    state.deck = [];
    state.gamblers = [];
  },
  clearDeck: (state: GameState) => {
    state.deck = deckService.initialDeck();
    state.gamblers.map(gambler => gambler.cards = []);
  },
  changeScene: (state: GameState, action: PayloadAction<Scene>) => {
    state.scene = action.payload;
  },
  placeBet: (state: GameState, action: PayloadAction<{ gamblerIndex: number, bet: number }>) => {
    const gamblerIndex = action.payload.gamblerIndex;
    state.gamblers[gamblerIndex].bet = action.payload.bet;
    state.gamblers[gamblerIndex].coins -= action.payload.bet;
  },
  drawCard: (state: GameState, action: PayloadAction<{ gamblerIndex: number, card: Card }>) => {
    const card = action.payload.card;
    const gamblerIndex = action.payload.gamblerIndex;
    const index = state.deck.findIndex(c => c.suit === card.suit && c.symbol === card.symbol);
    state.deck.splice(index, 1);
    state.gamblers[gamblerIndex].cards.push({ ...card, open: false });
  },
  swapCard: (state: GameState, action: PayloadAction<{ gamblerIndex: number, cardInhand: Card, cardInDeck: Card }>) => {
    const cardInhand = action.payload.cardInhand;
    const cardInDeck = action.payload.cardInDeck;
    const gamblerIndex = action.payload.gamblerIndex;
    const index = state.deck.findIndex(c => c.suit === cardInDeck.suit && c.symbol === cardInDeck.symbol);
    state.deck.splice(index, 1);
    state.deck.push(cardInhand);
    state.gamblers[gamblerIndex].cards.splice(1, 1);
    state.gamblers[gamblerIndex].cards.push({ ...cardInDeck, open: false });
  },
  openCard: (state: GameState, action: PayloadAction<{ gamblerIndex: number, cardIndex: number }>) => {
    const gamblerIndex = action.payload.gamblerIndex;
    const cardIndex = action.payload.cardIndex;
    state.gamblers[gamblerIndex].cards[cardIndex].open = true;
  },
  gameResult: (state: GameState, action: PayloadAction<{ gamblerIndex: number, coins: number }>) => {
    const gamblerIndex = action.payload.gamblerIndex;
    const coins = action.payload.coins;
    state.gamblers[gamblerIndex].bet = 0;
    state.gamblers[gamblerIndex].coins += coins;
  },
  addCriminal: (state: GameState, action: PayloadAction<{ gamblerIndex: number, criminal: Criminal }>) => {
    const gamblerIndex = action.payload.gamblerIndex;
    const criminal = action.payload.criminal;
    state.gamblers[gamblerIndex].criminals.push(criminal);
  }
};

// slice
const gameSlice = createSlice({
  name: GAME_STORE_NAMESPACE,
  initialState,
  reducers,
});

// selector
export const GameSelector = {
  scene: (state: RootState) => state.gameStore.scene,
  deck: (state: RootState) => state.gameStore.deck,
  gamblers: (state: RootState) => state.gameStore.gamblers,
  points: (state: RootState) => state.gameStore.gamblers.map(gambler => {
    return {
      name: gambler.name,
      point: gambler.cards.reduce((sum, card) => sum + card.point, 0) % 10,
      deng: (gambler.cards.every((card, _, arr) => card.suit === arr[0].suit) ||
        gambler.cards.every((card, _, arr) => card.symbol === arr[0].symbol))
          ? gambler.cards.length
          : 0
    };
  })
};

// action
export const GameAction = {
  newGame: gameSlice.actions.newGame,
  resetGame: gameSlice.actions.resetGame,
  clearDeck: gameSlice.actions.clearDeck,
  changeScene: gameSlice.actions.changeScene,
  placeBet: gameSlice.actions.placeBet,
  drawCard: gameSlice.actions.drawCard,
  swapCard: gameSlice.actions.swapCard,
  openCard: gameSlice.actions.openCard,
  gameResult: gameSlice.actions.gameResult,
  addCriminal: gameSlice.actions.addCriminal,
};

export default gameSlice.reducer;
